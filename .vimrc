runtime bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect()

"set colorscheme and syntax highlight
set background=dark
colorscheme solarized 

syntax enable
let g:php_sql_query=1
let g:php_htmlInStrings=1 
" Set extra options when running in GUI mode
if has("gui_running")
    if has ("gui_gtk2")
        set guifont=Monospace\ 9
        set guioptions-=T
    elseif has("gui_win32")
        set guifont=Consolas:h10:cANSI
    endif
else
    set guifont=Monospace\ 9
    set t_ut=
    set t_Co=256
    let g:solarized_termcolors=256
    "colorscheme distinguished
endif

"Fix indents
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set copyindent
set expandtab "Expand tabs to spaces


"Folding
set foldmethod=indent
set nofoldenable

"linenumber fix
set relativenumber
set cursorline
set nu

set ruler
set modeline
set ls=2

"Search conf
set incsearch
set showmatch
set hlsearch
set ignorecase smartcase
set wildmenu wildmode=list:full
set wildignore=*/.git/* 
:command Ss set sessionoptions=buffers|mksession! ~/currentSession.vim

"Change default folder for ~ and .swp files
if( expand($TEMP) == "" )
    "Default for linux
    let s:tempFolder = "/tmp"
else
    "Default for windows
    let s:tempFolder = expand($TEMP)
endif
let &directory=s:tempFolder
let &backupdir=s:tempFolder

"Allow backspace to do its thing
set backspace=indent,eol,start

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

"timestamps
map <F2> :echo 'Current time is ' . strftime('%c')<CR>
inoremap <F4> <C-R>=strftime("%H:%M:%S")<CR>
inoremap <F5> <C-R>=strftime("%d-%m-%Y")<CR>

inoremap jj <ESC>

set laststatus=2 
"set statusline=%f\%m\ buffer\:\ %n\ line\:\ %L\ encoding:\%{&fileencoding?&fileencoding:&encoding} 
set statusline=%f\%m\%r\%h\ %y\[buffer\:\%n\][lines\:\%L\]\ %=[%p%%]\[%l,%v]\[encoding\:\%{&fileencoding?&fileencoding:&encoding}]

" Prepare a new PHP class
function! Class()
    let name = input('Class name? ')
    let namespace = input('Any Namespace? ')

    if strlen(namespace)
        exec 'normal i<?php namespace ' . namespace
    else
        exec 'normal i<?php'
    endif

    " Open class
    exec 'normal oclass ' . name . ' {'
    exec 'normal lx'
    exec 'normal o'
    exec 'normal i  public function __construct(){'
    exec 'normal lx'
    exec 'normal o'
    exec 'normal o}'
    exec 'normal o}'
endfunction
nmap  ,1  :call Class()<cr>

"-------------------------
"syntax-------------------
"-------------------------
"Syntax highlight sql queries
let g:php_sql_query=1
"Syntax highlight html in strings
"let g:php_htmlInStrings=1
let g:rainbow_active = 0
"-------------------------
"custom mappings----------
"-------------------------
"insert php debug
nnoremap <F3> oecho "<pre style='background-color:white; text-align:left'>";<CR>var_dump();<ESC>oecho "</pre>";<ESC>k$hi
nnoremap gd vey:Ack! "function <C-R>""<CR>
nnoremap gc vey:Ack! "class <C-R>""<CR>

nnoremap <F5> :buffers<CR>:buffer<Space>

"-------------------------
"Navigate in wordwrap
"-------------------------
nnoremap j gj
nnoremap k gk

"Ctrl-J for new line
:nnoremap <NL> i<CR><ESC>

let mapleader=","

norm \ig
"-------------------------
"Plugin settings----------
"-------------------------
filetype plugin indent on
"NERDTree
"set autochdir "Always take make current directory root
let g:NERDTreeChDirMode=2
map <F8> :NERDTreeToggle<CR>
map <F7> :NERDTree /var/www/kunder/www.

"vim wiki
set nocompatible
filetype plugin on
nnoremap <C-c> :VimwikiToggleListItem<CR>

"ACK
"fix the open file thing...
let g:ackprg = "ack-grep -H --nogroup --column"

"Indent guides"
let g:indent_guides_enable_on_vim_startup = 1
hi IndentGuidesOdd  ctermbg=black
hi IndentGuidesEven ctermbg=green

"Indent guides"
nnoremap <silent> <leader>yw :call WindowSwap#MarkWindowSwap()<CR>
nnoremap <silent> <leader>pw :call WindowSwap#DoWindowSwap()<CR>
inoremap <expr><TAB> "\<C-n>"

set conceallevel=1
set concealcursor=nvi
autocmd VimEnter * syntax match spaces /  / conceal cchar= "yayw
autocmd BufWinEnter * syntax match spaces /  / conceal cchar= "yayw
