To add a new plugin as submodue:
Clone the thing!
git submodule add <REMOTE REPO> <LOCAL FOLDER>

To pull all plugins run:
git submodule init
git submodule update

To update all plugins:
git submodule foreach git pull origin master
